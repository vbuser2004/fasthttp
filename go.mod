module gitlab.com/vbuser2004/fasthttp

go 1.11

require (
	gitlab.com/vbuser2004/brotli v1.0.0
	gitlab.com/vbuser2004/compress v1.10.7
	gitlab.com/vbuser2004/bytebufferpool v1.0.0
	gitlab.com/vbuser2004/tcplisten v0.0.0-20161114210144-ceec8f93295a
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
	golang.org/x/net v0.0.0-20201016165138-7b1cca2348c0
	golang.org/x/sys v0.0.0-20200930185726-fdedc70b468f
)
